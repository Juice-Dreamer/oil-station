# oil-station

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### 项目目录
```
src
  assets：资源文件夹
    css
      normalize.css：是一个跨浏览器的通用css
    img
  common：一些公共的js文件
    const.js
    utils.js
  components：
    common：本项目以及其他项目可以使用的公共组件
    content：与当前项目业务相关的一些公共组件（如events和eventsForm都要用到的图）
  network：封装了网络请求相关
    request.js：封装axios
    events.js：与events页面有关的请求
  router：路由相关
  store：vuex相关--可能会按模块进行划分
    index.js：入口文件
    getter.js
  views
  App.vue
  main.js

```

