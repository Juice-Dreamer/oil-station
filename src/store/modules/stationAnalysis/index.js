import state from './state'
import getters from './getters'
import actions from './actions'
import mutaions from './mutations'

export default {
  namespaced:true,
  state,
  getters,
  actions,
  mutaions
}