import {GET_STATION_COMPREHENSIVE_SCORE} from './mutaion-types'
export default {
  [GET_STATION_COMPREHENSIVE_SCORE](state,{stationComprehensiveScore}){
    state.stationScore.comprehensive=stationComprehensiveScore
  }
}