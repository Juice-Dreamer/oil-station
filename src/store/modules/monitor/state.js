export default {
  stationScore: {
    comprehensive: 0, //加油站综合评分
    efficiency: 0, //加油站效率评分
    management: 0, //加油站管理评分
    security: 0, //加油站安全评分
    service: 0 //加油站服务评分
  }
}