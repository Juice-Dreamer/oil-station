import {reqStationServiceScore} from 'network/monitor'
import {GET_STATION_COMPREHENSIVE_SCORE} from './mutaion-types'
export default {
  async getStationServiceScore({commit},params) {//(context.commit,payload)
    let resData=await reqStationServiceScore(params);
    let stationComprehensiveScore =  resData[Object.keys(resData)[0]];
    commit(GET_STATION_COMPREHENSIVE_SCORE, { stationComprehensiveScore });
  } 
}