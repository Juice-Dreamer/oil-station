// vuex入口文件
// 1.安装vuex插件
import Vue from 'vue'
import Vuex from 'vuex'
import events from './modules/events/index'
import eventsForm from './modules/eventsForm/index'
import monitor from './modules/monitor/index'
import reality from './modules/reality/index'
import stationAnalysis from './modules/stationAnalysis/index'
Vue.use(Vuex)
// 2.实例化vuex对象
const store=new Vuex.Store({
  modules: {
    events,
    eventsForm,
    monitor,
    reality,
    stationAnalysis
  }
})
export default store