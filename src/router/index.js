// 路由入口文件

//1.路由插件安装
import Vue from 'vue'
import vueRouter from 'vue-router'
Vue.use(vueRouter)
    //2.引入基本组件--懒加载格式
const Main = () =>
    import ('views/main') //默认进入该页面的index
const Events = () =>
    import ('views/events')
const Monitor = () =>
    import ('views/monitor')
const Reality = () =>
    import ('views/reality')
const EventsForm = () =>
    import ('views/eventsForm')
const StationAnalysis = () =>
    import ('views/stationAnalysis')
    //3.配置路由
const routes = [{
        path: '/',
        redirect: '/monitor',
        component: Main,
        children: [{
                path: '/station-analysis',
                component: StationAnalysis
            },
            {
                path: '/monitor',
                component: Monitor
            },
            {
                path: '/events',
                component: Events
            },
            {
                path: '/reality',
                component: Reality
            },
            {
                path: '/form',
                component: EventsForm
            }
        ]
    }, ]
    //2.2避免多次点击路由
const originalPush = vueRouter.prototype.push
vueRouter.prototype.push = function push(location) {
    return originalPush.call(this, location).catch(err => err)
}
const originalReplace = vueRouter.prototype.replace
vueRouter.prototype.replace = function replace(location) {
        return originalReplace.call(this, location).catch(err => err)
    }
    //3.实例化路由对象并导出
const router = new vueRouter({
    routes,
    mode: 'history'
})

export default router