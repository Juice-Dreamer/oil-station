// 封装axios
import axios from 'axios'
export function request(config){
  //1.创建实例，初始化实例的全局配置
  const instance=axios.create({
    baseURL: 'http://oil.myhonor.top',
    timeout:5000
  })
  //2.设置拦截器
  instance.interceptors.request.use(config=>{
    console.log('请求拦截器生效')
    //主要是进行一些token设置
    sessionStorage.setItem("token","arUJ1zyLuV+nR+lhq774mPDHTwL5aKRW")
    if (sessionStorage.key("token") !== null) {
      config.headers.Authorization = `Bearer ${(
        sessionStorage.getItem("token") // 拼接字符串`aaa ${b}`=='aaa b'
      )}`;
      console.log('添加了token')
    }
    return config;//必须要返回，不然请求发送不出去
  },err=>{
    console.log(err)
  })
  instance.interceptors.response.use(res=>{
    console.log('响应拦截器生效')

    //主要是将返回的数据，进行一些处理
    return res.data
  },err=>{
    console.log(err)
  })
  //3.发送请求
  return instance(config)
}