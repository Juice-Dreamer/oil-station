import Vue from 'vue'
import App from './App.vue'
import router from './router/index'
import store from './store/index'
// 引入组件库
import VueUI from 'view-design'
import 'view-design/dist/styles/iview.css'
Vue.use(VueUI)
Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app')
